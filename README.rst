===================================================================
Brace Code -- simple project issue tracker written in Python/Django
===================================================================

Features
--------

(Not any of them is implemented yet, sorry.)

* tracking issues through every stage of development

* connection with your VCS of choice

* one place for your users' error reports

License
-------

BraceCode is licensed under BSD 3-Clause License.  For details, please look
into ``LICENSE.txt`` file.  Website templates and other binary content is `CC
3.0 BY-SA <http://creativecommons.org/licenses/by-sa/3.0/>`_, unless otherwise
stated.

Download
--------

For now just clone this repository.  Links to download should be available
after first release.

Requirements
------------

* Python 3.3
* PostgreSQL
* virtualenv

Installation
------------

Full installation procedure... Link not available yet.

#) Clone project's repository.

#) Create and switch to virtual environment for this project.

#) Install appropriate requirements. If you want to work on the project,
   install development dependencies: ``pip install -r requirements/local.txt``

#) If you'd rather just install and deploy Bracecode for yourself, install
   ``pip install -r requirements/production.txt``

Follow for deployment instructions... Link not available yet.

Changes
-------

v0.1
  basic support for multiple projects and their issues.
