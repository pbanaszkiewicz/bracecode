Installation
============

#) Download appropriate version or clone the main repository.

#) Install all the necessary dependencies.  You'd rather use virtual
   environment to store them.  ``pip install -r requirements/FILE.txt`` to
   install the dependencies.  ``FILE.txt`` is either ``production.txt`` or
   ``local.txt`` when you want development options.

#) Configure your database accordingly.  See
   ``bracecode/bracecode/settings/production`` or
   ``bracecode/bracecode/settings/local.py`` if you're interested in
   development

#) (From inside virtual environment) Load database schema:
   ``./manage.py sync`` and ``./manage.py migrate --all``.

Running tests
-------------

From inside virtual environment: ``./manage.py test``.

It's helpful to enable coverage reports when working on the project:
``coverage run manage.py test && coverage html``.  This command generates HTML
coverage report to ``bracecode/htmlcov``.  By default, it's browseable from
[http://localhost:8000/cov/index.html](http://localhost:8000/cov/index.html)


Compiling documentation
-----------------------

Simply run ``make html`` from inside ``docs`` directory in the main project
directory.
