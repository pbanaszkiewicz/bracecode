from django.contrib import admin
from .models import Project
# from issues.admin import IssueAdmin


class ProjectAdmin(admin.ModelAdmin):
    """Admin class for Project model."""
    date_hierarchy = "created"
    readonly_fields = ("name_slug", )
    list_display = ("name", )
    search_fields = ("name", "description")
    ordering = ("name", )
    # inlines = [
    #     IssueAdmin,
    # ]


admin.site.register(Project, ProjectAdmin)
