from django.db import models
from django.utils.translation import ugettext_lazy as _u
from model_utils.models import TimeStampedModel
from utils.utilities import slugify
# from issues.models import Issue


class Project(TimeStampedModel):
    """Model representing single Project in Brace Code.

    It inherits TimeStampedModel, so there're two additional model fields:
    ``created`` and ``modified``.

    TODO: URLs, git/hg, privacy settings
    TODO: users/groups working on a project
    """

    name = models.CharField(_u("Name"), max_length=255, db_index=True)
    name_slug = models.SlugField(_u("Name (slug)"), max_length=255, default="",
                                 editable=False)
    description = models.TextField(_u("Description"), blank=True)
    # issues_list = models.ManyToManyField(Issue, verbose_name=_u("Issues"),
    #                                      blank=True)

    def save(self, *args, **kwargs):
        """Make sure `name_slug` is indeed slug on save."""
        if not self.pk:
            self.name_slug = slugify(self.name)
        super(Project, self).save(*args, **kwargs)

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def __unicode__(self):
        return self.name
