from django.core.exceptions import ValidationError
from django.test import TestCase
from projects.models import Project


class TestProjectModel(TestCase):
    """Unit test Project model.

    There's no real need to test for creation/modification time, as it should
    be tested within django-model-utils tests, but I'm gonna leave this code
    for now.
    """

    def test_creation(self):
        """Test: Project different fields empty or not."""
        p1 = Project()
        p2 = Project(name="name")
        p3 = Project(name="name", description="")
        p4 = Project(name="name", description="text")

        with self.assertRaises(ValidationError):
            p1.full_clean()

        p2.full_clean()
        p3.full_clean()
        p4.full_clean()
        self.assertTrue(p1.created <= p2.created <= p3.created <= p4.created)

    def test_modification(self):
        """Test: Project modification time."""
        p1 = Project(name="name", description="")
        p2 = Project(name="name", description="text")
        p1.full_clean()
        p2.full_clean()

        p1.name = p2.name = "asdf"
        p1.full_clean()
        p2.full_clean()

        # apparently last modified time is set to now() whenever object is
        # created
        # rounding to 10^-4 is close enough on my computer, but I set 10^-3
        self.assertEqual(
            round(abs(p2.modified - p1.modified).total_seconds(), 3), 0
        )

    def test_verbose(self):
        """Test: Project model string representation methods (__unicode__,
        get_full_name, get_short_name).
        """
        project_name = "test_name"
        p1 = Project(name=project_name)

        self.assertEqual(p1.get_full_name(), project_name)
        self.assertEqual(p1.get_short_name(), project_name)
        self.assertEqual(unicode(p1), project_name)

