from django.views.generic.base import TemplateView
# from braces.views import LoginRequiredMixin
from utils.views import ContextTemplateView
from .models import Project


class ProjectAboutView(ContextTemplateView):
    template_name = "projects/about.html"

    def context(self):
        return dict(project=Project.objects.latest("created"))


class ProjectIssuesListView(TemplateView):
    template_name = "projects/issues_list.html"

    def get_context_data(self, **kw):
        context = super(ProjectIssuesListView, self).get_context_data(**kw)
        context["project"] = Project.objects.get(name_slug=kw["name_slug"])
        context["issues"] = context["project"].issue_set.all()
        return context
