from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
    url(r'^(?P<name_slug>[a-zA-Z0-9\-_]+)/$', views.ProjectAboutView.as_view(),
        name='project-about'),
    url(r'^(?P<name_slug>[a-zA-Z0-9\-_]+)/issues/$',
        views.ProjectIssuesListView.as_view(), name='project-issues'),
    url(r'^(?P<name_slug>[a-zA-Z0-9\-_]+)/milestones/$',
        views.ProjectAboutView.as_view(), name='project-milestones'),
    url(r'^(?P<name_slug>[a-zA-Z0-9\-_]+)/roadmap/$',
        views.ProjectAboutView.as_view(), name='project-roadmap'),
)
