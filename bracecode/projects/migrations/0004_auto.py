# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing M2M table for field issues_list on 'Project'
        db.delete_table('projects_project_issues_list')


    def backwards(self, orm):
        # Adding M2M table for field issues_list on 'Project'
        db.create_table(u'projects_project_issues_list', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'projects.project'], null=False)),
            ('issue', models.ForeignKey(orm[u'issues.issue'], null=False))
        ))
        db.create_unique(u'projects_project_issues_list', ['project_id', 'issue_id'])


    models = {
        u'projects.project': {
            'Meta': {'object_name': 'Project'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'name_slug': ('django.db.models.fields.SlugField', [], {'default': "''", 'max_length': '255'})
        }
    }

    complete_apps = ['projects']