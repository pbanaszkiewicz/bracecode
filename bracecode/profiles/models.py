from django.db import models
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser,
                                        PermissionsMixin)
from django.utils.translation import ugettext_lazy as _u
from model_utils.models import TimeStampedModel


class UserProfileManager(BaseUserManager):
    """Manager for UserProfile model."""

    def create_user(self, email, password=None):
        """Creates and saves user with specified email and optional
        password.
        """
        if not email:
            raise ValueError("User must have an email address.")

        user = self.model(email=UserProfileManager.normalize_email(email))
        user.set_password(password)
        user.full_clean()
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """Creates and saves superuser.  Both email and password are
        mandatory.
        """
        if not password:
            raise ValueError("Superuser must have a password.")
        user = self.create_user(email, password)
        user.is_admin = user.is_staff = user.is_superuser = True
        user.save(using=self._db)
        return user


class UserProfile(TimeStampedModel, AbstractBaseUser, PermissionsMixin):
    """Model used for django.contrib.auth user model."""
    email = models.EmailField(verbose_name=_u('Email address'),
                              max_length=255, unique=True, db_index=True)
    USERNAME_FIELD = 'email'

    is_active = models.BooleanField(default=True, verbose_name=_u('Active'))
    is_staff = models.BooleanField(default=False,
                                   verbose_name=_u('Staff member'))

    objects = UserProfileManager()

    class Meta:
        verbose_name = _u('user profile')
        verbose_name_plural = _u('user profiles')

    def get_full_name(self):
        """Identify user by it's email only."""
        return self.email

    def get_short_name(self):
        """Also identify user by it's email."""
        return self.email

    def __unicode__(self):
        return self.email
