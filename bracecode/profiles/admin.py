from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import UserProfile
from django.utils.translation import ugettext_lazy as _u


class UserProfileCreationForm(forms.ModelForm):
    """A form for creating new users.  Includes all the required fields, plus
    a repeated password.
    """
    password1 = forms.CharField(label=_u('Password'),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_u('Password (repeat)'),
                                widget=forms.PasswordInput)

    class Meta:
        model = UserProfile
        fields = ("email", )

    def clean_password2(self):
        """Check that the two passwords match each other."""
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_u("Passwords don't match."))
        return password2

    def save(self, commit=True):
        user = super(UserProfileCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get("password1"))
        if commit:
            user.save()
        return user


class UserProfileChangeForm(forms.ModelForm):
    """A form for changing existing users.  Includes all the required fields,
    but replaces the password field with admin's password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = UserProfile

    def clean_password(self):
        return self.initial.get("password")


class UserProfileAdmin(UserAdmin):
    """Admin class for UserProfile model (django.contrib.auth.models.user
    replacemenet).
    """
    add_form = UserProfileCreationForm
    form = UserProfileChangeForm

    date_hierarchy = "created"

    list_editable = ("is_active", "is_superuser", "is_staff")
    list_display = ("email", "is_active", "is_staff", "is_superuser",)
    list_filter = ("is_active", "is_staff", "is_superuser", "groups",
                   "last_login", "created", "modified")
    search_fields = ("email", )
    ordering = ("email", )
    filter_horizontal = ("groups", "user_permissions")

    fieldsets = (
        (None, {"fields": ("email", "password")}),
        ("Permissions", {"fields": ("is_active", "is_staff", "is_superuser",
                                    "groups", "user_permissions")}),
        ("Important dates", {"fields": ("last_login", )})
    )

    add_fieldsets = (
        (None, {
            "classes": ("wide", ),
            "fields": ("email", "password1", "password2"),
        }),
    )


admin.site.register(UserProfile, UserProfileAdmin)
