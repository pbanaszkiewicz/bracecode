from django.core.exceptions import ValidationError
from django.test import TestCase
from profiles.models import UserProfile


class TestUserProfileModel(TestCase):
    """Unit test UserProfile model."""

    def tearDown(self):
        UserProfile.objects.all().delete()

    def test_creation(self):
        """Test: UserProfile different fields empty or not."""

        u1 = UserProfile()
        u2 = UserProfile(email="wrong_email")
        u3 = UserProfile(email="good@email.com")
        u4 = UserProfile(email="wrong_email", password="a")
        u5 = UserProfile(email="good@email.com", password="a")

        with self.assertRaises(ValidationError):
            u1.full_clean()
        with self.assertRaises(ValidationError):
            u2.full_clean()
        with self.assertRaises(ValidationError):
            u3.full_clean()
        with self.assertRaises(ValidationError):
            u4.full_clean()

        u5.full_clean()

    def test_create_user(self):
        """Test: UserProfile.objects.create_user()"""

        with self.assertRaises(ValidationError):
            UserProfile.objects.create_user(email="wrong_email")
        with self.assertRaises(ValidationError):
            UserProfile.objects.create_user(email="wrong_email", password="a")
        with self.assertRaises(ValueError):
            UserProfile.objects.create_user("")

        u2 = UserProfile.objects.create_user(email="good@email.com")
        u4 = UserProfile.objects.create_user(email="better@email.com",
                                             password="a")

    def test_create_superuser(self):
        """Test: UserProfile.objects.create_superuser()"""

        with self.assertRaises(ValidationError):
            UserProfile.objects.create_superuser(email="wrong_email",
                                                 password="a")
        with self.assertRaises(ValueError):
            UserProfile.objects.create_superuser("", "")
        with self.assertRaises(ValueError):
            UserProfile.objects.create_superuser("some@email.com", "")

        user_password = "strong_password"
        u1 = UserProfile.objects.create_superuser(email="better@email.com",
                                                  password=user_password)

        self.assertNotEqual(u1.password, user_password)
        self.assertTrue(u1.is_admin)
        self.assertTrue(u1.is_staff)
        self.assertTrue(u1.is_active)

    def test_verbose(self):
        """Test: UserProfile string representation methods (__str__, __repr__,
        __unicode__, get_full_name, get_short_name)."""

        email, password = "good@email.com", "strong_password"
        u1 = UserProfile.objects.create_user(email, password)

        self.assertEqual(u1.get_full_name(), email)
        self.assertEqual(u1.get_short_name(), email)
        self.assertEqual(unicode(u1), email)
