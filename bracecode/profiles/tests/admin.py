from datetime import datetime
from django.core.exceptions import ValidationError
from django.test import TestCase
from profiles.models import UserProfile
from profiles.admin import UserProfileCreationForm, UserProfileChangeForm

class TestUserProfileAdmin(TestCase):
    """Unit test UserProfile admin (creation/change) forms."""

    def tearDown(self):
        UserProfile.objects.all().delete()

    def test_creation_form(self):
        """Test: UserProfileCreationForm can create users.

        Test against malformed data (not matching passwords and/or wrong email
        address) and then against good input.

        Lastly, ensure that saved user's password doesn't match input password
        (i.e. is encrypted).
        """
        user_password = "strong_password"

        data_wrong1 = {"email": "good@email.com", "password1": user_password,
                       "password2": user_password + "1"}
        data_wrong2 = {"email": "wrong_email", "password1": user_password,
                       "password2": user_password}
        data_right = {"email": "good@email.com", "password1": user_password,
                      "password2": user_password}

        with self.assertRaises(ValueError):
            UserProfileCreationForm(data_wrong1).save()

        with self.assertRaises(ValueError):
            UserProfileCreationForm(data_wrong2).save()

        UserProfileCreationForm(data_right).save()
        self.assertEqual(len(UserProfile.objects.all()), 1)

        self.assertNotEqual(UserProfile.objects.latest("pk").password,
                            user_password)

    def test_change_form(self):
        "Test: UserProfileChangeForm can edit users, but not their passwords."

        email, password = "good@email.com", "strong_password"
        weak_password = "weak"
        u1 = UserProfile.objects.create_superuser(email, password)

        data_wrong1 = {"email": "wrong_email", "last_login": datetime.now()}
        data_wrong2 = {"email": "better@email.com", "password": weak_password,
                       "last_login": datetime.now()}
        data_right = {"email": "better@email.com",
                      "last_login": datetime.now()}

        with self.assertRaises(ValueError):
            UserProfileChangeForm(instance=u1, data=data_wrong1).save()

        UserProfileChangeForm(instance=u1, data=data_wrong2).save()
        self.assertNotEqual(u1.password, weak_password)

        UserProfileChangeForm(instance=u1, data=data_right).save()

