from django.contrib import admin
from django.conf.urls import patterns
from .models import Issue


class IssueAdmin(admin.ModelAdmin):
    date_hierarchy = "created"
    list_editable = ("kind", "status", "priority", "assignment")
    list_display = ("subject", "number", "project", "kind", "status",
                    "priority", "assignment")
    list_filter = ("kind", "status", "created", "modified")
    search_fields = ("subject", "description")
    readonly_fields = ("number", )
    ordering = ("project", "-number")


admin.site.register(Issue, IssueAdmin)
