from django.db import models
from django.utils.translation import ugettext_lazy as _u
from django.utils.translation import ugettext as _
from model_utils import Choices
from model_utils.models import TimeStampedModel
from profiles.models import UserProfile
from projects.models import Project


class Issue(TimeStampedModel):
    """Model for single issue.

    TODO: category, tags
    TODO: difficulty
    TODO: additional fields (hstore?)
    TODO: time spent
    TODO: triage version IMPORTANT
    TODO: comments
    """

    project = models.ForeignKey(Project, verbose_name=_u("Project"))

    KIND_CHOICES = Choices(
        (0, "bug", _("Bug")),
        (1, "feature", _("Feature request")),
        (2, "enhancement", _("Enhancement")),
    )
    STATUS_CHOICES = Choices(
        (0, "new", _("New")),
        (1, "progress", _("In progress")),
        (2, "review", _("Needs review")),
        (3, "work", _("Needs work")),
        (4, "resolved", _("Resolved")),
    )
    PRIORITY_CHOICES = Choices(
        (0, "low", _("Low")),
        (1, "normal", _("Normal")),
        (2, "high", _("High")),
        (3, "urgent", _("Urgent")),
    )

    number = models.IntegerField(_u("Issue #"), editable=False)

    kind = models.IntegerField(_u("Type"), choices=KIND_CHOICES,
                               default=KIND_CHOICES.bug)

    status = models.IntegerField(_u("Status"), choices=STATUS_CHOICES,
                                 default=STATUS_CHOICES.new)

    priority = models.IntegerField(_u("Priority"), choices=PRIORITY_CHOICES,
                                   default=PRIORITY_CHOICES.normal)

    subject = models.CharField(_u("Subject"), max_length=255)
    description = models.TextField(_u("Description"))
    assignment = models.ForeignKey(UserProfile, verbose_name=_u("Assigned to"),
                                   blank=True, null=True)

    def save(self, *args, **kwargs):
        """Overriding this method to provide auto-incrementing issues numbers
        across specified project.
        """
        if not self.number:
            self.number = self.project.issue_set.latest("number").number + 1
        super(Issue, self).save(*args, **kwargs)
