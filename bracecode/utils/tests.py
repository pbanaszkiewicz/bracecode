# coding: utf-8
from django.test import TestCase
from utilities import slugify

class TestUtilities(TestCase):
    """Unit test utilities included in ``utils`` app."""

    def test_slugify(self):
        """Test: slugify works for various input data."""

        data = (
            (u"Zażółć gęślą jaźń", u"zazolc-gesla-jazn"),
            (u"The quick brown fox jumps over the lazy dog",
             u"the-quick-brown-fox-jumps-over-the-lazy-dog"),
            (u"My custom хелло ворлд", u"my-custom-khello-vorld"),
            (u"Mørdag Æther", u"mordag-aether"),
        )

        for s1, s2 in data:
            self.assertEqual(slugify(s1), s2)
