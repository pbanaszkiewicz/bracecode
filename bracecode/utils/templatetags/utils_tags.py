from django import template
from django.core.urlresolvers import reverse

register = template.Library()

@register.simple_tag(takes_context=True)
def active_url(context, urlname, *aargs, **kkwargs):
    url = reverse(urlname, args=aargs, kwargs=kkwargs)
    if url == context["request"].get_full_path():
        return 'class="active"'
    return ''
