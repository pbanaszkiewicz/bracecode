# Create your views here.
from django.views.generic import TemplateView

class ContextTemplateView(TemplateView):
    """Add context to your template view easier, omit all unnecessary super()
    calls.
    """

    def context(self):
        raise NotImplementedError("You have to override this method.")

    def get_context_data(self, **kwargs):
        context = super(ContextTemplateView, self).get_context_data(**kwargs)

        context.update(self.context())
        return context
