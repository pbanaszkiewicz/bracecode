# coding: utf-8
import re
from unidecode import unidecode

def slugify(s):
    """Make proper latin, URL-safe slugs.  Works actually better than
    ``django.template.defaultfilters.slugify`` which has some corner cases.
    """
    return re.sub(r'\W+', '-', unidecode(s).lower())
