from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS += (
    ("Piotr Banaszkiewicz", "piotr@banaszkiewicz.org"),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bracecode',
        'USER': 'django',
        'PASSWORD': 'django',
        'HOST': 'vagrant',
        'PORT': '',
    }
}

INSTALLED_APPS += ("debug_toolbar", "autofixture")
INTERNAL_IPS = ("127.0.0.1", )
MIDDLEWARE_CLASSES += ("debug_toolbar.middleware.DebugToolbarMiddleware", )

DEBUG_TOOLBAR_CONFIG = {"INTERCEPT_REDIRECTS": False}
