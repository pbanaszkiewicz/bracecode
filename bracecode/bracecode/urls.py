from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bracecode.views.home', name='home'),
    # url(r'^bracecode/', include('bracecode.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Dashboard views
    url(r'^dashboard/', include("dashboard.urls")),

    # Project views
    url(r'^project/', include("projects.urls")),
)

# serve documentation files only during development
if settings.DEBUG or settings.TESTING:
    urlpatterns += static('/docs/', document_root=settings.DOCUMENTATION_ROOT)
    urlpatterns += static('/cov/', document_root=settings.COVERAGE_ROOT)
