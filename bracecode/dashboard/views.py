from braces.views import LoginRequiredMixin
from utils.views import ContextTemplateView

class DashboardView(LoginRequiredMixin, ContextTemplateView):
    template_name = "dashboard/dashboard.html"

    def context(self):
        return dict(msg="Dupa.14")
